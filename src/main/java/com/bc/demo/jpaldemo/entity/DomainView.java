package com.bc.demo.jpaldemo.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class DomainView {

    private Long d1Id;
    private Long d2Id;
    private String linkage;
}
