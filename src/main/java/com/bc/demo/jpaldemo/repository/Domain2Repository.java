package com.bc.demo.jpaldemo.repository;

import com.bc.demo.jpaldemo.entity.Domain2;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface Domain2Repository extends CrudRepository<Domain2, Long> {
}
