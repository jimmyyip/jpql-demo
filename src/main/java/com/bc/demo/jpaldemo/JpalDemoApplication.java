package com.bc.demo.jpaldemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JpalDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(JpalDemoApplication.class, args);
    }

}
