package com.bc.demo.jpaldemo.repository;

import com.bc.demo.jpaldemo.entity.Domain1;
import com.bc.demo.jpaldemo.entity.Domain2;
import com.bc.demo.jpaldemo.entity.DomainView;
import lombok.extern.slf4j.Slf4j;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@SpringBootTest
@ActiveProfiles("test")
@Slf4j
public class Domain1RepositoryTest {

    @Autowired
    private Domain1Repository domain1Repository;

    @Autowired
    private Domain2Repository domain2Repository;

    @Test
    public void testCreate() {
        domain1Repository.deleteAll();
        domain2Repository.deleteAll();

        Domain1 domain1A = new Domain1();
        domain1A.setLinkage("linkage");
        domain1Repository.save(domain1A);
        Domain1 domain1B = new Domain1();
        domain1B.setLinkage("linkage");
        domain1Repository.save(domain1B);
        Domain2 domain2A = new Domain2();
        domain2A.setLinkage("linkage");
        domain2Repository.save(domain2A);
        Domain2 domain2B = new Domain2();
        domain2B.setLinkage("linkage");
        domain2Repository.save(domain2B);

        Page<DomainView> page = domain1Repository.findByLinkage("linkage", PageRequest.of(1, 2));
        Assert.assertEquals(page.getTotalPages(), 2);
        Assert.assertEquals(page.getNumberOfElements(), 2);
        Assert.assertEquals(page.getTotalElements(), 4);

        log.info("page = {}", page);
    }

}