package com.bc.demo.jpaldemo.repository;

import com.bc.demo.jpaldemo.entity.Domain1;
import com.bc.demo.jpaldemo.entity.DomainView;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface Domain1Repository extends CrudRepository<Domain1, Long> {

    @Query("SELECT new com.bc.demo.jpaldemo.entity.DomainView(d1.id, d2.id, d1.linkage) FROM Domain1 d1, Domain2 d2 WHERE d1.linkage = :linkage AND d1.linkage = d2.linkage")
    Page<DomainView> findByLinkage(@Param("linkage") String linkage, Pageable pageable);
}
